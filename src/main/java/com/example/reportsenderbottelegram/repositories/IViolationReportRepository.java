package com.example.reportsenderbottelegram.repositories;

import com.example.reportsenderbottelegram.models.ViolationReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IViolationReportRepository extends JpaRepository<ViolationReport, Long> {

    @Query("select v from ViolationReport v where v.timestamp > ?1 and (v.deviceGroupId = ?2 or v.deviceGroupId like concat(?2, '%'))")
    List<ViolationReport> findViolationReportByTimestampAfterAndDeviceGroupId(Long last24Hours, String group);


    @Query("select v from ViolationReport v where v.timestamp > ?1")
    List<ViolationReport> findViolationReportByTimestampAfter(Long last24Hours);
}
