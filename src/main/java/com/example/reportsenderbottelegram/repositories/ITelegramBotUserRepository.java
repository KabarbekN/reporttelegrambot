package com.example.reportsenderbottelegram.repositories;

import com.example.reportsenderbottelegram.models.TelegramBotUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ITelegramBotUserRepository extends JpaRepository<TelegramBotUser, Long> {
    @Query("select t from TelegramBotUser t where t.phoneNumber = ?1")
    TelegramBotUser findByPhoneNumber(String phoneNumber);

    @Query("select t from TelegramBotUser t where t.chatId = ?1")
    TelegramBotUser findByChatId(String chatId);


    @Query("select t from TelegramBotUser t where t.group = ?1")
    List<TelegramBotUser> findByGroup(String group);



    /*
    то есть если будет 0101 то сработает первая проверка и он вытащит только такой же результат
    но даже если инпут будет 0102 то он все равно посмотрит первые два значения и вытащит пользователей с id 01
    последняя проверка будет брать самого главного, в данном случае считается что он будет записан как '00'
     */
    @Query("select t from TelegramBotUser t where t.group = ?1 or t.group like substring(?1, 1, 2) or t.group = '00'")
    List<TelegramBotUser> findByGroupContains(String group);





}
