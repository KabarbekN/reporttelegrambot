package com.example.reportsenderbottelegram.components;

import com.example.reportsenderbottelegram.configurations.BotConfiguration;
import com.example.reportsenderbottelegram.models.TelegramBotUser;
import com.example.reportsenderbottelegram.models.ViolationReport;
import com.example.reportsenderbottelegram.models.ViolationType;
import com.example.reportsenderbottelegram.services.TelegramBotUserService;
import com.example.reportsenderbottelegram.services.ViolationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.commands.SetMyCommands;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.commands.BotCommand;
import org.telegram.telegrambots.meta.api.objects.commands.scope.BotCommandScopeDefault;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class TelegramBot extends TelegramLongPollingBot {
    private final BotConfiguration botConfiguration;


    private final ViolationService violationService;
    private final TelegramBotUserService botUserService;
//    private final ViolationPdfService violationPdfService;

    @Override
    public void onUpdateReceived(Update update) {

        if (update.hasMessage()) {
            addBotCommands(update.getMessage().getChatId());
        }

        if (update.hasCallbackQuery()) {
            try {
//                sendPdf(update.getCallbackQuery().getMessage().getChatId(), update.getCallbackQuery().getData());
                System.out.println("Send pdf");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        if (update.hasMessage() && update.getMessage().hasContact()){
            registerHandler(update);
        }

        if (update.hasMessage() && update.getMessage().hasText() && !update.getMessage().hasContact()){
            String messageText = update.getMessage().getText();
            Long chatId = update.getMessage().getChatId();

            switch (messageText){
                case "/start":
                    System.out.println(System.currentTimeMillis());
                    if (checkRegistered(chatId)){
                        sendMessageWithKeyboard(chatId, "Выберите метод из меню", basicCommandKeyboard());
                    }else {
                        registerInDB(update);
                        sendMessageWithKeyboard(chatId, "Выберите метод из меню", basicCommandKeyboard());
                    }
                    break;
                case "Получить репорт за день", "/report":
                    sendLast24HoursReport(chatId);
                    break;
                case "":
                    sendMessage(chatId, "Используйте команды");
                default:
                    sendMessage(chatId, "Данной команды не существует");
            }
        }
    }


    /*


    Нужно дописать метод sendLast24HoursReport



     */

    public void sendLast24HoursReport(Long chatId){
        TelegramBotUser user = botUserService.getUser(chatId);
        if(user != null) {
            List<ViolationReport> reportsInLast24Hours = violationService.getViolationLast24Hours(user.getGroup());
            System.out.println(reportsInLast24Hours);
            if (reportsInLast24Hours.size() != 0) {
                sendMessageWithKeyboard(chatId, "Репорты за последние 24 часа", basicCommandKeyboard());
                for (ViolationReport report : reportsInLast24Hours) {
                        String result = reportBuilder(report);
                        send(chatId, result, latLong2Gis(report.getLatitude(), report.getLongitude()), report.getId());
                }
            } else {
                sendMessageWithKeyboard(chatId, "За последние 24 часа нету репортов", basicCommandKeyboard());
            }
        }
    }

    public String reportBuilder(ViolationReport report){
        StringBuilder result = new StringBuilder();

        result.append("Номер ТС: ").append(report.getDeviceId()).append('\n');
        result.append("Тип нарушения: ").append(report.getViolationType()).append('\n');
        if (report.getViolationType().equals(ViolationType.SPEED_VIOLATION))
            result.append("Скорость: ").append(report.getSpeed()).append("км/ч").append('\n');
        if (report.getViolationType().equals(ViolationType.TIME_VIOLATION)) {
            result.append("Время начала: ").append(timestampToDate(report.getTimestamp())).append('\n');
            result.append("Время окончания: ").append(timestampToDate(report.getTimestamp())).append('\n');
        }
        result.append("Геопозиция: ").append(report.getLatitude()).append(" / ").append(report.getLongitude()).append('\n');
        result.append("Дата и время: ").append(timestampToDate(report.getTimestamp()));
        return String.valueOf(result);
    }

    public void registerHandler(Update update){
        if(update.getMessage().hasContact()){
            TelegramBotUser user = botUserService.findUser(update.getMessage().getContact().getPhoneNumber());
            System.out.println(user);
            if (user != null){
                log.info(user.toString());
                user.setPhoneNumber(update.getMessage().getContact().getPhoneNumber());
                user.setFirstname(update.getMessage().getContact().getFirstName());
                user.setLastname(update.getMessage().getContact().getLastName());
                user.setChatId(String.valueOf(update.getMessage().getChatId()));
                user.setCreationTime(System.currentTimeMillis() / 1000);
                log.info(user.toString());
                botUserService.addUser(user);
                sendMessageWithKeyboard(update.getMessage().getChatId(), "Вы успешно зарегистрировались! Добро пожаловать в систему GPS.", basicCommandKeyboard());
            }
            else{
                sendMessageWithKeyboard(update.getMessage().getChatId(),"Недоступно", basicCommandKeyboard());
            }
        }
    }



//    private void sendPdf(Long chatId, String callbackData) throws Exception {
//        Long id = Long.valueOf(callbackData.split("_")[1]);
//        var violation = violationService.findById(id);
//        if (violation.isPresent()) {
//            var outputStream = violationPdfService.generatePdf(violation.get());
//            ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
//            InputFile inputFile = new InputFile(inputStream, "report" + id + ".pdf");
//
//            SendDocument sendDocument = new SendDocument();
//            sendDocument.setChatId(String.valueOf(chatId));
//            sendDocument.setDocument(inputFile);
//            sendDocument.setCaption("Нарушение №" + id);
//
//            try {
//                execute(sendDocument);
//            } catch (TelegramApiException e) {
//                e.printStackTrace();
//            }
//        }
//    }

    @Scheduled(fixedRate = 1800000, initialDelay = 1000)
    private void reportsToString() {
        List<ViolationReport> reports = violationService.getViolationsLast10Min();
        if (reports != null)
            log.info("List size = {}", reports.size());
        else
            log.info("Empty list");

        if (reports != null){
            for (ViolationReport report: reports) {
                List<TelegramBotUser> telegramBotUsers =
                        botUserService.getAllUsersByGroupAndGroupCityAndHeadOfDepartment(report.getDeviceGroupId());
                if (telegramBotUsers != null){
                    for (TelegramBotUser telegramBotUser: telegramBotUsers) {
                        if (telegramBotUser.getChatId() != null) {
                            Long chatId = Long.valueOf(telegramBotUser.getChatId());
                            String result = reportBuilder(report);
                            send(chatId, result, latLong2Gis(report.getLatitude(), report.getLongitude()), report.getId());
                        }
                    }
                }
            }
        }
    }
    public boolean checkRegistered(Long chatId){
        TelegramBotUser user = botUserService.getUser(chatId);
        return user != null;
    }

    private String latLong2Gis(Double lat, Double lng) {
        return "https://2gis.ru/maps/geo/" + lng + "," + lat + "?m=" + lng + "," + lat;
    }

    private String timestampToDate(Long timestamp) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        Date date = new Date(timestamp * 1000);
        return simpleDateFormat.format(date);
    }

    private void registerInDB(Update update) {
        SendMessage sm = new SendMessage();
        Long chatID = update.getMessage().getChatId();
        sm.setChatId(String.valueOf(chatID));

        if (botUserService.getUser(chatID) == null) {
//            sm.setChatId(String.valueOf(chatID));
            sm.setText("Для продолжения работы нужна регистрация");

            ReplyKeyboardMarkup markup = new ReplyKeyboardMarkup();
            List<KeyboardRow> keyboard = new ArrayList<>();

            KeyboardRow row = new KeyboardRow();
            KeyboardButton keyboardButton = new KeyboardButton();
            keyboardButton.setText("Зарегистрироваться");
            keyboardButton.setRequestContact(true);
            row.add(keyboardButton);
            keyboard.add(row);

            markup.setKeyboard(keyboard);
            markup.setResizeKeyboard(true);

            sm.setReplyMarkup(markup);
        } else {
            sm.setText("Вы уже зарегистрированы");

        }
        try {
            execute(sm);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }


    public void sendMessage(Long chatId, String text){
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(chatId));
        sendMessage.setText(text);
        try {
            execute(sendMessage);
        }
        catch (TelegramApiException e){
            log.error("Error occurred with sending message " + e.getMessage());
        }
    }

    public void sendMessageWithKeyboard(Long chatId, String text,ReplyKeyboardMarkup replyKeyboardMarkup){
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(chatId));
        sendMessage.setText(text);
        sendMessage.setReplyMarkup(replyKeyboardMarkup);

        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            log.error("Error occurred with sendingMessageWithKeyboard " + e.getMessage());
        }
    }

    public ReplyKeyboardMarkup basicCommandKeyboard(){
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(true);
        List<KeyboardRow> keyboardRows = new ArrayList<>();
        KeyboardRow row1 = new KeyboardRow();
        row1.add("Получить репорт за день");
//        row1.add("Тіркелу");

        keyboardRows.add(row1);

        replyKeyboardMarkup.setKeyboard(keyboardRows);
        return replyKeyboardMarkup;
    }


    public void send(Long chatID, String text, String link, Long violationId) {
        SendMessage s = new SendMessage();
        s.setText(text);
        s.setChatId(chatID.toString());
        if (link != null) {
            InlineKeyboardButton keyboardButton = new InlineKeyboardButton();
            keyboardButton.setUrl(link);
            keyboardButton.setText("Открыть на карте");

            InlineKeyboardButton keyboardButton1 = new InlineKeyboardButton();
            keyboardButton1.setText("Скачать PDF");
            keyboardButton1.setCallbackData("downloadpdf_" + violationId);

            InlineKeyboardMarkup keyboardMarkup = new InlineKeyboardMarkup();
            keyboardMarkup.setKeyboard(List.of(List.of(keyboardButton, keyboardButton1)));
            s.setReplyMarkup(keyboardMarkup);
        }
        try {
            execute(s);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }


    private void addBotCommands(Long chatId){
        List<BotCommand> botCommands = new ArrayList<>();

        botCommands.add(new BotCommand("/start", "Запустить бота"));
        botCommands.add(new BotCommand("/report", "Получить репорты за последние 24 часа"));
        try {
            this.execute(new SetMyCommands(botCommands, new BotCommandScopeDefault(), null));
        } catch (TelegramApiException e) {
            log.error("Error setting bot's command list occurred: " + e.getMessage());
        }
    }

    @Override
    public String getBotUsername() {
        return botConfiguration.getBotName();
    }

    @Override
    public String getBotToken() {
        return botConfiguration.getBotToken();
    }
}
