package com.example.reportsenderbottelegram;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class ReportSenderBotTelegramApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReportSenderBotTelegramApplication.class, args);
    }

}
