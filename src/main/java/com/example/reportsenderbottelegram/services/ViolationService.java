package com.example.reportsenderbottelegram.services;

import com.example.reportsenderbottelegram.models.ViolationReport;
import com.example.reportsenderbottelegram.repositories.IViolationReportRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ViolationService {

    private final IViolationReportRepository violationReportRepository;
    public List<ViolationReport> getViolationsLast10Min() {
        long endTimestamp = System.currentTimeMillis() / 1000;
        long startTimestamp = (endTimestamp - 1800);
//        List<ViolationReport> result = violationReportRepository.findAllByStartTimestampBetween(startTimestamp, endTimestamp);
        List<ViolationReport> result = violationReportRepository.findAll();
        return result;
    }

    public List<ViolationReport> getViolationLast24Hours(String group){

        long representationOf24Hours =  24 * 60 * 60 * 1000;
        Long last24Hours = System.currentTimeMillis() - representationOf24Hours;

        if (group.equals("00")) return violationReportRepository.findViolationReportByTimestampAfter(last24Hours);
        return violationReportRepository.findViolationReportByTimestampAfterAndDeviceGroupId(last24Hours, group);
    }
}
