package com.example.reportsenderbottelegram.services;

import com.example.reportsenderbottelegram.models.TelegramBotUser;
import com.example.reportsenderbottelegram.repositories.ITelegramBotUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class TelegramBotUserService {
    private final ITelegramBotUserRepository telegramBotUserRepository;
    public void addUser(TelegramBotUser user) {
        telegramBotUserRepository.save(user);
    }

    public TelegramBotUser findUser(String phoneNumber) {
return telegramBotUserRepository.findByPhoneNumber(phoneNumber);
    }

    public TelegramBotUser getUser(Long chatID) {
     return telegramBotUserRepository.findByChatId(String.valueOf(chatID));}

    public List<TelegramBotUser> getAllUsers(){
        return telegramBotUserRepository.findAll();
    }

    public List<TelegramBotUser> getAllUsersByGroupAndGroupCityAndHeadOfDepartment(String group){
        return telegramBotUserRepository.findByGroupContains(group);
    }
}
