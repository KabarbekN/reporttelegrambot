package com.example.reportsenderbottelegram.models;

public enum ViolationType {
    SPEED_VIOLATION,
    TIME_VIOLATION,
    GEO_ZONE_VIOLATION,
    INACTIVITY_VIOLATION
}
