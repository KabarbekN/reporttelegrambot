package com.example.reportsenderbottelegram.models;

import lombok.Data;

import jakarta.persistence.*;
import java.util.Objects;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;


@Data
@SuperBuilder
@Entity
@Table(name = "violation_report")
@AllArgsConstructor
@NoArgsConstructor
public class ViolationReport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "account_id")
    private String accountId;
    @Column(name = "device_id")
    private String deviceId;
    @Column(name = "device_group_id")
    private String deviceGroupId;
    @Column(name = "latitude")
    private Double latitude;
    @Column(name = "longitude")
    private Double longitude;
    @Column(name = "timestamp")
    private Long timestamp;
    @Column(name = "odometerKM")
    private Double odometerKM;
    @Column(name = "status_code")
    private String statusCode;
    @Column(name = "violation_type")
    @Enumerated(EnumType.STRING)
    private ViolationType violationType;
    @Column(name = "start_timestamp")
    private Long startTimestamp;
    @Column(name = "end_timestamp")
    private Long endTimestamp;
    @Column(name = "speed")
    private Integer speed;

}